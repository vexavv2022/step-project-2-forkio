"use strict";
const {src, dest, watch, series, parallel} = require("gulp");
const autoprefixer = require("gulp-autoprefixer");
const clean = require("gulp-clean");
const cleanCSS = require("gulp-clean-css");
const concat = require("gulp-concat");
const purgeCSS = require("gulp-purgecss")
const uglify = require("gulp-uglify");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass")(require("sass"));
const imagemin = require("gulp-imagemin-fix");


const files = {
    htmlPath: "./index.html",
    scssPath: "./src/scss/**/*.scss",
    jsPath: "./src/js/*",
    imgPath: "./src/img/**/*"
}
const clearDist = () => {
    return src("./dist/**/*", {read: false}).pipe(clean());
}
function scssTask() {
    return src(files.scssPath)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat("style.min.css"))
        .pipe(autoprefixer())
        .pipe(cleanCSS({compatibility: "ie8"}))
        .pipe(purgeCSS({
            content: ['./*.html']
        }))
        .pipe(dest('./dist/css/'))
}

function jsTask() {
    return src(files.jsPath)
        .pipe(uglify())
        .pipe(concat("scripts.min.js"))
        .pipe(dest("./dist/js/"))

}

function imgTask() {
    return src(files.imgPath)
        .pipe(imagemin())
        .pipe(dest("./dist/img"));
}

function browserSyncServe(cb) {
    browserSync.init({
        server: {
            baseDir: '.'
        }
    });
    cb();
}

function watchTask() {
    watch('*.html').on('change', browserSync.reload);
    watch(files.scssPath, scssTask).on('change', browserSync.reload);
    watch(files.jsPath, jsTask).on('change', browserSync.reload);
    watch(files.imgPath, imgTask).on('change', browserSync.reload);
}


const build = series(clearDist, parallel(scssTask, jsTask, imgTask));
const dev = series(browserSyncServe, watchTask);
exports.clearDist = clearDist;
exports.scssTask = scssTask;
exports.jsTask = jsTask;
exports.imgTask = imgTask;
exports.watchTask = watchTask;
exports.build = build;
exports.dev = dev;
exports.default = series(clearDist, parallel(scssTask, jsTask, imgTask), browserSyncServe, watchTask);

